﻿export default [
  {
    path: '/login',
    layout: false,
    routes: [
      {
        name: '登录',
        path: '/login',
        component: './login',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/index',
    name: 'welcome',
    icon: 'smile',
    component: './index',
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    // access: 'canAdmin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './index',
      },
      {
        component: './404',
      },
    ],
  },
  {
    name: 'list.table-list',
    icon: 'table',
    path: '/list',
    component: './TableList',
  },
  {
    path: '/menu',
    name: '路由管理',
    component: './menu',
  },
  {
    path: '/',
    redirect: '/index',
  },
  {
    component: './404',
  },
];
