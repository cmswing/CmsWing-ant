// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 添加组织  添加组织  POST /adminApi/cw_sys/addOrg */
export async function AdminapiCwSysAddorg(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysAddorgParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.addOrgRequest,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/addOrg`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 添加角色  添加角色  POST /adminApi/cw_sys/addRole */
export async function AdminapiCwSysAddrole(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysAddroleParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.addRoleRequest,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/addRole`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 添加用户  添加用户  POST /adminApi/cw_sys/addUser */
export async function AdminapiCwSysAdduser(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysAdduserParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.userItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/addUser`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 获取用户信息  登录后获取用户的登录信息  GET /adminApi/cw_sys/currentUser */
export async function AdminapiCwSysCurrentuser(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysCurrentuserParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/currentUser`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 删除组织  删除组织  GET /adminApi/cw_sys/delOrg */
export async function AdminapiCwSysDelorg(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysDelorgParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/delOrg`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 删除角色  删除角色  GET /adminApi/cw_sys/delRole */
export async function AdminapiCwSysDelrole(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysDelroleParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/delRole`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 删除用户  删除用户  GET /adminApi/cw_sys/delUser */
export async function AdminapiCwSysDeluser(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysDeluserParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/delUser`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 组织拖动排序  组织拖动排序  POST /adminApi/cw_sys/dropOrg */
export async function AdminapiCwSysDroporg(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysDroporgParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.orgItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/dropOrg`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 编辑组织  编辑组织  POST /adminApi/cw_sys/editOrg */
export async function AdminapiCwSysEditorg(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysEditorgParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.orgItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/editOrg`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 编辑角色  编辑角色  POST /adminApi/cw_sys/editRole */
export async function AdminapiCwSysEditrole(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysEditroleParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.roleTtem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/editRole`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 编辑用户  编辑用户  POST /adminApi/cw_sys/editUser */
export async function AdminapiCwSysEdituser(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysEdituserParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.userItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/editUser`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 获取所有角色  获取所有角色  GET /adminApi/cw_sys/getAllRole */
export async function AdminapiCwSysGetallrole(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysGetallroleParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/getAllRole`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 组织列表  组织列表  GET /adminApi/cw_sys/orgList */
export async function AdminapiCwSysOrglist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysOrglistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/orgList`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取角色列表  获取角色列表  GET /adminApi/cw_sys/roleList */
export async function AdminapiCwSysRolelist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysRolelistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/roleList`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取全部树路由节点  获取全部树路由节点  GET /adminApi/cw_sys/roleNode */
export async function AdminapiCwSysRolenode(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysRolenodeParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/roleNode`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取角色详情  获取角色详情  GET /adminApi/cw_sys/showRole */
export async function AdminapiCwSysShowrole(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysShowroleParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_sys/showRole`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取用户列表  获取用户列表  GET /adminApi/cw_sys/userList */
export async function AdminapiCwSysUserlist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwSysUserlistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.userListResponse>(`${API_URL}/adminApi/cw_sys/userList`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}
