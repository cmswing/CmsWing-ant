// @ts-ignore
/* eslint-disable */
// API 更新时间：
// API 唯一标识：
import * as cms from './cms';
import * as cwDev from './cwDev';
import * as cwLogin from './cwLogin';
import * as cwSys from './cwSys';
export default {
  cms,
  cwDev,
  cwLogin,
  cwSys,
};
