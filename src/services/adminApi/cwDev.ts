// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 添加模块  添加模块  POST /adminApi/cw_dev/addMod */
export async function AdminapiCwDevAddmod(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevAddmodParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.modRequest,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/addMod`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 添加路由  添加路由  POST /adminApi/cw_dev/addRouter */
export async function AdminapiCwDevAddrouter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevAddrouterParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.menuItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/addRouter`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 删除模块  删除模块  GET /adminApi/cw_dev/delMod */
export async function AdminapiCwDevDelmod(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevDelmodParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/delMod`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 删除路由  删除路由  GET /adminApi/cw_dev/delRouter */
export async function AdminapiCwDevDelrouter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevDelrouterParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/delRouter`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 编辑模块  编辑模块  POST /adminApi/cw_dev/editMod */
export async function AdminapiCwDevEditmod(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevEditmodParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.modRequest,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/editMod`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 编辑路由  编辑路由  POST /adminApi/cw_dev/editRouter */
export async function AdminapiCwDevEditrouter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevEditrouterParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.menuItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/editRouter`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 获取全部树路由节点  获取全部树路由节点  GET /adminApi/cw_dev/getAllTreeMenu */
export async function AdminapiCwDevGetalltreemenu(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGetalltreemenuParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/getAllTreeMenu`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取控制器列表  获取控制器列表  GET /adminApi/cw_dev/getController */
export async function AdminapiCwDevGetcontroller(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGetcontrollerParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/getController`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 获取控制器方法  获取控制器方法  GET /adminApi/cw_dev/getControllerFun */
export async function AdminapiCwDevGetcontrollerfun(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGetcontrollerfunParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/getControllerFun`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取系统菜单  获取系统菜单  GET /adminApi/cw_dev/getMenu */
export async function AdminapiCwDevGetmenu(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGetmenuParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/getMenu`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 获取中间件  获取项目中自定义的中间件  GET /adminApi/cw_dev/getMiddleware */
export async function AdminapiCwDevGetmiddleware(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGetmiddlewareParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.menuResponse>(`${API_URL}/adminApi/cw_dev/getMiddleware`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 获取模块详情  获取模块详情  GET /adminApi/cw_dev/getModInfo */
export async function AdminapiCwDevGetmodinfo(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGetmodinfoParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.modeInfoResponse>(`${API_URL}/adminApi/cw_dev/getModInfo`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取模块列表  获取模块列表  GET /adminApi/cw_dev/getModList */
export async function AdminapiCwDevGetmodlist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGetmodlistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.modeListResponse>(`${API_URL}/adminApi/cw_dev/getModList`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 获取路由列表  获取路由列表  GET /adminApi/cw_dev/getTreeMenu */
export async function AdminapiCwDevGettreemenu(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevGettreemenuParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.menuResponse>(`${API_URL}/adminApi/cw_dev/getTreeMenu`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 重启服务器  重启服务器  GET /adminApi/cw_dev/restart */
export async function AdminapiCwDevRestart(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevRestartParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/restart`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 获取上传配置  获取上传配置  GET /adminApi/cw_dev/uploadConfig */
export async function AdminapiCwDevUploadconfig(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevUploadconfigParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/uploadConfig`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 编辑上传配置  编辑上传配置  POST /adminApi/cw_dev/uploadEdit */
export async function AdminapiCwDevUploadedit(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevUploadeditParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.uploadEdit,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/uploadEdit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 获取上传编辑表单  获取上传编辑表单  GET /adminApi/cw_dev/uploadSchema */
export async function AdminapiCwDevUploadschema(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCwDevUploadschemaParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cw_dev/uploadSchema`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
