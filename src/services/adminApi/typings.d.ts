declare namespace API {
  type accountRequest = {
    /** 用户名 */
    username: string;
    /** 密码 */
    password: string;
    /** 类型 */
    type: string;
  };

  type accountResponse = {
    success: boolean;
    token: string;
    type?: string;
    msg: string;
    isLogin: boolean;
  };

  type addOrgRequest = {
    name: string;
    desc?: string;
    pid: number;
    sort?: number;
  };

  type addRoleRequest = {
    name: string;
    desc?: string;
    menu_ids: string;
    state?: boolean;
  };

  type AdminapiCmsAttraddParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsAttrdelParams = {
    /** 用户token  */
    token?: string;
    /** 字段id  */
    id: number;
  };

  type AdminapiCmsAttreditParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsAttrlistParams = {
    /** 用户token  */
    token?: string;
    /** desc  */
    mod_id: number;
  };

  type AdminapiCmsFormattraddParams = {
    /** 用户token  */
    token?: string;
    /** 模型Id  */
    mod_id: number;
    /** 字段id  */
    attr_id: number;
    /** 字段别名  */
    alias: number;
  };

  type AdminapiCmsFormattrdelParams = {
    /** 用户token  */
    token?: string;
    /** 字段Id  */
    id: number;
  };

  type AdminapiCmsFormattreditParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsFormattrlistParams = {
    /** 用户token  */
    token?: string;
    /** 模型Id  */
    mod_id: number;
  };

  type AdminapiCmsFormattrselectParams = {
    /** 用户token  */
    token?: string;
    /** 模型id  */
    mod_id: number;
  };

  type AdminapiCmsFormschemaParams = {
    /** 用户token  */
    token?: string;
    /** 模型id  */
    mod_id: number;
  };

  type AdminapiCmsFormsortParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsModaddParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsModdelParams = {
    /** 用户token  */
    token?: string;
    /** desc  */
    '': number;
  };

  type AdminapiCmsModeditParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsModinfoParams = {
    /** 用户token  */
    token?: string;
    /** 模型id  */
    id: number;
  };

  type AdminapiCmsModlistParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsTableattraddParams = {
    /** 用户token  */
    token?: string;
    /** 模型Id  */
    mod_id: number;
    /** 字段id  */
    attr_id: number;
    /** 字段别名  */
    alias: number;
  };

  type AdminapiCmsTableattrdelParams = {
    /** 用户token  */
    token?: string;
    /** 字段Id  */
    id: number;
  };

  type AdminapiCmsTableattreditParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsTableattrselectParams = {
    /** 用户token  */
    token?: string;
    /** 模型id  */
    mod_id: number;
  };

  type AdminapiCmsTableschemaParams = {
    /** 用户token  */
    token?: string;
    /** 模型id  */
    mod_id: number;
  };

  type AdminapiCmsTablesortParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCmsTalbeattrlistParams = {
    /** 用户token  */
    token?: string;
    /** 模型Id  */
    mod_id: number;
  };

  type AdminapiCwDevAddmodParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevAddrouterParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevDelmodParams = {
    /** 用户token  */
    token?: string;
    /** 模块id  */
    id: number;
  };

  type AdminapiCwDevDelrouterParams = {
    /** 用户token  */
    token?: string;
    /** 路由id  */
    id: number;
  };

  type AdminapiCwDevEditmodParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevEditrouterParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevGetalltreemenuParams = {
    /** 用户token  */
    token?: string;
    /** 模块id  */
    mod_id: number;
  };

  type AdminapiCwDevGetcontrollerfunParams = {
    /** 用户token  */
    token?: string;
    /** 控制器名称  */
    c: number;
  };

  type AdminapiCwDevGetcontrollerParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevGetmenuParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevGetmiddlewareParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevGetmodinfoParams = {
    /** 用户token  */
    token?: string;
    /** 模块id  */
    id: number;
  };

  type AdminapiCwDevGetmodlistParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevGettreemenuParams = {
    /** 用户token  */
    token?: string;
    /** 模块id  */
    mod_id?: number;
    /** 是否后端  */
    admin?: boolean;
    /** 路由名称  */
    name?: string;
    /** 路由url  */
    path?: string;
    /** 控制器  */
    controller?: string;
    /** 控制器  */
    action?: string;
    /** 请求方式  */
    verb?: string;
    /** 中间件  */
    middleware?: string;
    /** 排除中间件  */
    ignoreMiddleware?: string;
    /** 当此节点被选中的时候也会选中 parentKeys 的节点  */
    parentKeys?: string;
    /** 权限配置，需要预先配置权限  */
    access?: string;
  };

  type AdminapiCwDevRestartParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevUploadconfigParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevUploadeditParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwDevUploadschemaParams = {
    /** 用户token  */
    token?: string;
    /** 类型  */
    type: string;
  };

  type AdminapiCwSysAddorgParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysAddroleParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysAdduserParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysCurrentuserParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysDelorgParams = {
    /** 用户token  */
    token?: string;
    /** id  */
    id: number;
  };

  type AdminapiCwSysDelroleParams = {
    /** 用户token  */
    token?: string;
    /** id  */
    id: number;
  };

  type AdminapiCwSysDeluserParams = {
    /** 用户token  */
    token?: string;
    /** 用户id  */
    id: number;
  };

  type AdminapiCwSysDroporgParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysEditorgParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysEditroleParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysEdituserParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysGetallroleParams = {
    /** 用户token  */
    token?: string;
  };

  type AdminapiCwSysOrglistParams = {
    /** 用户token  */
    token?: string;
    /** 如果有值返回全树  */
    t?: string;
  };

  type AdminapiCwSysRolelistParams = {
    /** 用户token  */
    token?: string;
    /** current  */
    current: string;
    /** pageSize  */
    pageSize: string;
    /** name  */
    name?: string;
    /** state  */
    state?: string;
    /** desc  */
    desc?: string;
    /** sorter  */
    sorter?: string;
  };

  type AdminapiCwSysRolenodeParams = {
    /** 用户token  */
    token?: string;
    /** 模块id  */
    mod_id: number;
  };

  type AdminapiCwSysShowroleParams = {
    /** 用户token  */
    token?: string;
    /** 角色id  */
    id: number;
  };

  type AdminapiCwSysUserlistParams = {
    /** 用户token  */
    token?: string;
  };

  type arrtItem = {
    id?: number;
    name?: string;
    title?: string;
    field?: string;
    type?: string;
    is_show?: number;
    mod_id?: number;
    is_must?: boolean;
    status?: boolean;
    remark?: string;
  };

  type baseResponse = {
    success: boolean;
    data: string;
    errorCode: string;
    errorMessage: string;
  };

  type menuItem = {
    id?: number;
    name?: string;
    path?: string;
    icon?: string;
    access?: string;
    hideChildrenInMenu?: boolean;
    hideInMenu?: boolean;
    hideInBreadcrumb?: boolean;
    headerRender?: boolean;
    footerRender?: boolean;
    menuRender?: boolean;
    menuHeaderRender?: boolean;
    flatMenu?: boolean;
    parentKeys?: string;
    verb?: 'head' | 'options' | 'get' | 'put' | 'post' | 'patch' | 'del' | 'redirect' | 'resources';
    middleware?: string;
    ignoreMiddleware?: string;
    controller?: string;
    action?: string;
    admin?: boolean;
    mod_id?: number;
    pid?: number;
    sort?: number;
    children?: menuItem[];
  };

  type menuResponse = {
    success: boolean;
    data: menuItem[];
    errorCode: string;
    errorMessage: string;
  };

  type modeInfoResponse = {
    success: boolean;
    data: modRequest;
    errorCode: string;
    errorMessage: string;
  };

  type modeListResponse = {
    success: boolean;
    data: modRequest[];
    errorCode: string;
    errorMessage: string;
  };

  type modItem = {
    id?: number;
    name?: string;
    title?: string;
    extend?: number;
    status?: boolean;
    remark?: string;
  };

  type modRequest = {
    id?: number;
    name: string;
    path: string;
    middleware?: string;
    remarks?: string;
    sort?: number;
  };

  type orgItem = {
    id: number;
    name: string;
    desc?: string;
    pid: number;
    sort?: number;
  };

  type roleTtem = {
    id: number;
    name: string;
    desc?: string;
    menu_ids: string;
    state?: boolean;
  };

  type tokenRequest = {
    /** 用户token */
    token: string;
  };

  type uploadEdit = {
    data?: string;
  };

  type userItem = {
    id?: number;
    username?: string;
    password?: string;
    email?: string;
    mobile?: string;
    state?: number;
    org_id?: number;
    admin?: boolean;
  };

  type userListResponse = {
    success: boolean;
    total: number;
    data: userItem[];
  };
}
