// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 用户登录  用户登录接口  POST /adminApi/cw_login/account */
export async function AdminapiCwLoginAccount(
  body: API.accountRequest,
  options?: { [key: string]: any },
) {
  return request<API.accountResponse>(`${API_URL}/adminApi/cw_login/account`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
