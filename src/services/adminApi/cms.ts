// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 添加字段  添加字段  POST /adminApi/cms/attrAdd */
export async function AdminapiCmsAttradd(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsAttraddParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.arrtItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/attrAdd`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 删除字段  删除字段  GET /adminApi/cms/attrDel */
export async function AdminapiCmsAttrdel(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsAttrdelParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/attrDel`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 编辑字段  编辑字段  POST /adminApi/cms/attrEdit */
export async function AdminapiCmsAttredit(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsAttreditParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.arrtItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/attrEdit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 获取字段  获取字段  GET /adminApi/cms/attrList */
export async function AdminapiCmsAttrlist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsAttrlistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/attrList`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 表单设计添加字段  表单设计添加字段  GET /adminApi/cms/formAttrAdd */
export async function AdminapiCmsFormattradd(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsFormattraddParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/formAttrAdd`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 表单设计删除字段  表单设计删除字段  GET /adminApi/cms/formAttrDel */
export async function AdminapiCmsFormattrdel(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsFormattrdelParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/formAttrDel`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 表单设计编辑字段  表单设计编辑字段  POST /adminApi/cms/formAttrEdit */
export async function AdminapiCmsFormattredit(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsFormattreditParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: string,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/formAttrEdit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 表单设计字段列表  表单设计字段列表  GET /adminApi/cms/formAttrList */
export async function AdminapiCmsFormattrlist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsFormattrlistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/formAttrList`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 表单设计已选择的字段  已选择的字段  GET /adminApi/cms/formAttrSelect */
export async function AdminapiCmsFormattrselect(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsFormattrselectParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/formAttrSelect`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 根据模型生成表单Schema  根据模型生成表单Schema  GET /adminApi/cms/formSchema */
export async function AdminapiCmsFormschema(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsFormschemaParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/formSchema`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 表单设计字段排序  表单设计字段排序  POST /adminApi/cms/formSort */
export async function AdminapiCmsFormsort(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsFormsortParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: string,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/formSort`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 添加模型  添加模型  POST /adminApi/cms/modAdd */
export async function AdminapiCmsModadd(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsModaddParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.modItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/modAdd`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 删除模型  删除模型  GET /adminApi/cms/modDel */
export async function AdminapiCmsModdel(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsModdelParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/modDel`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 编辑模型  编辑模型  POST /adminApi/cms/modEdit */
export async function AdminapiCmsModedit(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsModeditParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: API.modItem,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/modEdit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 获取模型信息  根据id取货模型信息  GET /adminApi/cms/modInfo */
export async function AdminapiCmsModinfo(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsModinfoParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/modInfo`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 获取模型列表  获取模型列表  GET /adminApi/cms/modList */
export async function AdminapiCmsModlist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsModlistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/modList`, {
    method: 'GET',
    headers: {},
    params: { ...params },
    ...(options || {}),
  });
}

/** 列表设计添加字段  列表设计添加字段  GET /adminApi/cms/tableAttrAdd */
export async function AdminapiCmsTableattradd(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsTableattraddParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/tableAttrAdd`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 列表设计删除字段  列表设计删除字段  GET /adminApi/cms/tableAttrDel */
export async function AdminapiCmsTableattrdel(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsTableattrdelParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/tableAttrDel`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 列表设计编辑字段  列表设计编辑字段  POST /adminApi/cms/tableAttrEdit */
export async function AdminapiCmsTableattredit(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsTableattreditParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: string,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/tableAttrEdit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 列表设计已选择的字段  已选择的字段  GET /adminApi/cms/tableAttrSelect */
export async function AdminapiCmsTableattrselect(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsTableattrselectParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/tableAttrSelect`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 根据模型生成列表Schema  根据模型生成列表Schema  GET /adminApi/cms/tableSchema */
export async function AdminapiCmsTableschema(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsTableschemaParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/tableSchema`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 列表设计字段排序  列表设计字段排序  POST /adminApi/cms/tableSort */
export async function AdminapiCmsTablesort(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsTablesortParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  body: string,
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/tableSort`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

/** 列表设计字段列表  列表设计字段列表  GET /adminApi/cms/talbeAttrList */
export async function AdminapiCmsTalbeattrlist(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AdminapiCmsTalbeattrlistParams & {
    // header
    /** 用户token  */
    token?: string;
  },
  options?: { [key: string]: any },
) {
  return request<API.baseResponse>(`${API_URL}/adminApi/cms/talbeAttrList`, {
    method: 'GET',
    headers: {},
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
