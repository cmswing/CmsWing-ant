import { request } from '@umijs/max';

export default {
  async iconfont() {
    return await request('/public/admin/iconfont/iconfont.json');
  },
};
