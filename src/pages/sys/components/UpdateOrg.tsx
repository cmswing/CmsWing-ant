import adminApi from '@/services/adminApi';
import {
  ModalForm,
  ProForm,
  ProFormDigit,
  ProFormText,
  ProFormTreeSelect,
} from '@ant-design/pro-components';
import React, { useEffect, useState } from 'react';
export type FormValueType = {
  target?: string;
  template?: string;
  type?: string;
  time?: string;
  frequency?: string;
} & Partial<API.RuleListItem>;

export type UpdateFormProps = {
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: FormValueType) => Promise<void>;
  updateModalVisible: number;
  values: any;
};
const UpdateOrg: React.FC<UpdateFormProps> = (props) => {
  const [values, setValues] = useState<any>({});
  useEffect(() => {
    if (props.updateModalVisible == 2) {
      const v: any = {};
      v.name = props?.values?.title;
      v.pid = props?.values?.pid;
      v.desc = props?.values?.desc;
      v.sort = props?.values?.sort;
      v.id = props?.values?.id;
      setValues(v);
    } else {
      setValues({});
    }
  }, [props]);

  return (
    <ModalForm
      title={props.updateModalVisible == 1 ? '新建组织' : '编辑组织'}
      visible={props.updateModalVisible != 0}
      autoFocusFirstInput
      modalProps={{
        onCancel: () => props.onCancel(),
        maskClosable: false,
        destroyOnClose: true,
      }}
      onFinish={async (value) => {
        console.log(value);
        if (values.id) {
          value.id = values.id;
        }
        props.onSubmit(value);
        return true;
      }}
      initialValues={values}
    >
      <ProForm.Group>
        <ProFormText
          width="md"
          name="name"
          label="组织名称"
          tooltip="最长为 24 位"
          placeholder="请输入名称"
          rules={[{ required: true }]}
        />
        <ProFormTreeSelect
          width="md"
          name="pid"
          label="上级路由"
          rules={[{ required: true }]}
          request={async (): Promise<any> => {
            const data = await adminApi.cwSys.AdminapiCwSysOrglist({ t: 'true' });
            return data.data;
          }}
          tooltip="如果是一级菜单请选择一级菜单"
          placeholder="请选择上级路由"
          fieldProps={{
            fieldNames: {
              label: 'title',
              value: 'key',
            },
            showSearch: true,
            treeNodeFilterProp: 'title',
          }}
        />
      </ProForm.Group>
      <ProFormText name="desc" label="组织说明" tooltip="请输入组织说明" />
      <ProFormDigit
        width="xs"
        label="排序"
        name="sort"
        tooltip="数字越小越靠前"
        placeholder="0"
        fieldProps={{ precision: 0 }}
      />
    </ModalForm>
  );
};
export default UpdateOrg;
