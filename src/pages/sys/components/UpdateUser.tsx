import adminApi from '@/services/adminApi';
import {
  ModalForm,
  ProForm,
  ProFormSelect,
  ProFormSwitch,
  ProFormText,
  ProFormTreeSelect,
} from '@ant-design/pro-components';
import React, { useEffect, useState } from 'react';

export type UpdateFormProps = {
  onCancel: (flag?: boolean) => void;
  onSubmit: (values: API.userItem) => Promise<void>;
  visible: number;
  values: API.userItem & { cw_roles?: any };
};
const UpdateUser: React.FC<UpdateFormProps> = (props) => {
  const [isAdmin, setIsAdmin] = useState<boolean>(false);
  const [userrow, setUserRow] = useState({});
  useEffect(() => {
    console.log(props.values);
    const v: any = {};
    if (props.visible == 2) {
      v.username = props?.values?.username;
      v.email = props?.values?.email;
      v.mobile = props?.values?.mobile;
      v.org_id = props?.values?.org_id ? props?.values?.org_id : '';
      v.admin = props?.values?.admin;
      v.state = props?.values?.state;
      v.role_ids = props?.values?.cw_roles;
      v.id = props?.values?.id;
    } else {
      v.admin = false;
      v.state = true;
    }
    setIsAdmin(v.admin);
    setUserRow(v);
  }, [props]);
  return (
    <ModalForm
      title={props.visible == 1 ? '新建用户' : '编辑用户'}
      visible={props.visible != 0}
      autoFocusFirstInput
      modalProps={{
        onCancel: () => {
          props.onCancel();
          setUserRow({});
        },
        maskClosable: false,
        destroyOnClose: true,
      }}
      onFinish={async (value) => {
        console.log(value);
        if (props.visible == 2) {
          value.id = props?.values?.id;
        }
        props.onSubmit(value);
        return true;
      }}
      initialValues={userrow || {}}
    >
      <ProForm.Group>
        <ProFormText
          width="md"
          name="username"
          label="用户名称"
          tooltip="最长为 24 位"
          placeholder="请输入名称"
          rules={[{ required: true }]}
        />
        <ProFormText
          width="md"
          name="password"
          label="登录密码"
          tooltip="修改时如果不填登录密码就是不修改"
          placeholder="请输入登录密码"
          rules={[{ required: props.visible == 1 }]}
        />
        <ProFormText
          width="md"
          name="email"
          label="邮箱"
          tooltip="最长为 24 位"
          placeholder="请输入邮箱"
          rules={[{ required: true }]}
        />
        <ProFormText
          width="md"
          name="mobile"
          label="手机号"
          tooltip="手机号"
          placeholder="请输入手机号码"
          rules={[{ required: true }]}
        />
        <ProFormTreeSelect
          width="md"
          name="org_id"
          label="所属组织"
          request={async (): Promise<any> => {
            const data: any = await adminApi.cwSys.AdminapiCwSysOrglist({ t: 'true' });
            data.data?.shift();
            return data.data;
          }}
          tooltip="请选择组织"
          placeholder="请选择组织"
          fieldProps={{
            fieldNames: {
              label: 'title',
              value: 'key',
            },
            showSearch: true,
            treeNodeFilterProp: 'title',
          }}
        />
        <ProFormSwitch
          width="xs"
          name="admin"
          label="系统管理员"
          tooltip="系统管理员会跳过所有权限"
          fieldProps={{
            onChange: (e) => {
              setIsAdmin(e);
            },
          }}
        />
        <ProFormSwitch width="xs" name="state" label="启用" tooltip="启用" />
      </ProForm.Group>
      {!isAdmin && (
        <ProFormSelect
          name="role_ids"
          label="角色权限"
          request={async () => {
            const data: any = await adminApi.cwSys.AdminapiCwSysGetallrole({});
            return data.data;
          }}
          tooltip="系统管理员无需设置角色"
          placeholder="请选择角色权限"
          fieldProps={{ mode: 'multiple', labelInValue: true }}
          rules={[{ required: true }]}
        />
      )}
    </ModalForm>
  );
};
export default UpdateUser;
