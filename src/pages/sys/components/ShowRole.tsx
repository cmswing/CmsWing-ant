import adminApi from '@/services/adminApi';
import { Badge, Descriptions, Divider, Drawer, Tree } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
const ShowRole: React.FC<{ id: any; visible: boolean; onClose?: () => void }> = (props) => {
  const [show, setShow] = useState<any>();
  const getInfos = useCallback(() => {
    if (props.id) {
      return adminApi.cwSys
        .AdminapiCwSysShowrole({ id: props.id })
        .then((res) => setShow(res.data));
    }
    return props.id;
  }, [props.id]);
  useEffect(() => {
    getInfos();
  }, [getInfos]);
  return (
    <Drawer
      title={show?.name}
      placement="right"
      visible={props.visible}
      onClose={props.onClose}
      size="large"
    >
      <Descriptions column={2}>
        <Descriptions.Item label="角色名称">{show?.name}</Descriptions.Item>
        <Descriptions.Item label="角色状态">
          {show?.state ? (
            <Badge status="processing" text="启用" />
          ) : (
            <Badge status="error" text="禁用" />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="角色描述" span={2}>
          {show?.desc}
        </Descriptions.Item>
      </Descriptions>
      <Divider />
      <Descriptions title="权限节点">
        <Descriptions.Item>
          {show && (
            <Tree
              showLine={{ showLeafIcon: false }}
              defaultExpandAll={true}
              treeData={show?.tree}
              fieldNames={{ title: 'name', key: 'id' }}
            />
          )}
        </Descriptions.Item>
      </Descriptions>
    </Drawer>
  );
};
export default ShowRole;
