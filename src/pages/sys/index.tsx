import { history } from '@umijs/max';
import React, { useEffect } from 'react';
const Index: React.FC = () => {
  useEffect(() => {
    history.push('/sys/user');
  }, []);
  return <></>;
};
export default Index;
