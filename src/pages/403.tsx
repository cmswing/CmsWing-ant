import { history, useSearchParams } from '@umijs/max';
import { Button, Result } from 'antd';
import React from 'react';
const NoFoundPage: React.FC = () => {
  const [searchParams] = useSearchParams();
  return (
    <>
      <Result
        status="403"
        title="对不起，您无权访问此页面。"
        subTitle={`接口:${searchParams.get('url')}`}
        extra={
          <Button type="primary" onClick={() => history.push('/')}>
            返回首页
          </Button>
        }
      />
    </>
  );
};

export default NoFoundPage;
