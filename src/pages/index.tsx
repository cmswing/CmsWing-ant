import { PageContainer } from '@ant-design/pro-components';
import { FormattedMessage, useIntl, useModel } from '@umijs/max';
import { Alert, Card, Typography } from 'antd';
import React from 'react';
import styles from './index.less';

const CodePreview: React.FC = ({ children }) => (
  <pre className={styles.pre}>
    <code>
      <Typography.Text copyable>{children}</Typography.Text>
    </code>
  </pre>
);

const Index: React.FC = () => {
  const intl = useIntl();
  //权限使用
  //调用全局数据
  const { initialState } = useModel('@@initialState');
  //节点验证方法 initialState?.ver?.(rid) rid 是节点的id,在后台 /admin/dev/menu 路由管理查找
  //返回 true有权限/false无权限
  console.log(initialState?.ver?.(29));
  return (
    <PageContainer>
      <Card>
        <Alert
          message={intl.formatMessage({
            id: 'pages.welcome.alertMessage',
            defaultMessage: 'Faster and stronger heavy-duty components have been released.',
          })}
          type="success"
          showIcon
          banner
          style={{
            margin: -12,
            marginBottom: 24,
          }}
        />
        <Typography.Text strong>
          <a
            href="https://procomponents.ant.design/components/table"
            rel="noopener noreferrer"
            target="__blank"
          >
            <FormattedMessage id="pages.welcome.link" defaultMessage="Welcome" />
          </a>
        </Typography.Text>
        <CodePreview>
          {initialState?.ver?.(29)
            ? '有权限 initialState?.ver?.(29)'
            : '没有权限 initialState?.ver?.(29)'}
        </CodePreview>
      </Card>
    </PageContainer>
  );
};

export default Index;
