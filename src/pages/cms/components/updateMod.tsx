import {
  ModalForm,
  ProForm,
  ProFormDigit,
  ProFormRadio,
  ProFormSwitch,
  ProFormText,
} from '@ant-design/pro-components';
import React from 'react';

export type UpdateFormProps = {
  onCancel: (flag?: boolean) => void;
  onSubmit: (values: API.modItem) => Promise<void>;
  visible: number;
  values: API.modItem;
};
const UpdateMod: React.FC<UpdateFormProps> = (props) => {
  return (
    <ModalForm
      title={props.visible == 1 ? '新建模型' : '编辑模型'}
      visible={props.visible != 0}
      autoFocusFirstInput
      modalProps={{
        onCancel: (can: any) => {
          props.onCancel(can);
        },
        maskClosable: false,
        destroyOnClose: true,
      }}
      onFinish={async (value) => {
        console.log(value);
        if (props.visible == 2) {
          value.id = props?.values?.id;
        }
        props.onSubmit(value);
        return true;
      }}
    >
      <ProForm.Group>
        <ProFormText
          width="md"
          name="title"
          label="模型名称"
          tooltip="模型名称"
          placeholder="请输入模型名称"
          initialValue={props?.values?.title}
          rules={[{ required: true }]}
        />
        <ProFormText
          width="md"
          name="name"
          label="模型标识"
          tooltip="表名,只能是小写字母"
          placeholder="请输入模型标识"
          readonly={props.visible == 2}
          initialValue={props?.values?.name}
          rules={[{ required: true }]}
        />
        <ProFormRadio.Group
          width="md"
          name="extend"
          label="继承模型"
          options={[
            {
              label: '基础模型',
              value: '1',
            },
          ]}
          rules={[{ required: true }]}
          initialValue="1"
          tooltip="系统模型会继承基础模型"
        />
        <ProFormDigit
          width="xs"
          name="list_row"
          label="列表数据长度"
          tooltip="默认每页20条"
          placeholder="请输入列表长度"
          initialValue={props?.values?.list_row ? props?.values?.list_row : 20}
          min={0}
        />
        <ProFormSwitch
          width="sm"
          name="status"
          initialValue={props.visible == 2 ? props?.values?.status : true}
          label="启用/禁用"
          tooltip="启用/禁用"
        />
        <ProFormDigit
          width="xs"
          name="level"
          label="优先级"
          tooltip="优先级（越高排序越靠前）"
          placeholder="请输入优先级"
          initialValue={props.visible == 2 ? props?.values?.level : 0}
          min={0}
        />
      </ProForm.Group>
      <ProFormText name="remark" label="模块描述" tooltip="模块描述" placeholder="请输入模块描述" />
    </ModalForm>
  );
};
export default UpdateMod;
