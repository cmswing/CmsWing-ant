// FormDesign
import adminApi from '@/services/adminApi';
import {
  EditOutlined,
  EyeOutlined,
  ReloadOutlined,
  SortAscendingOutlined,
} from '@ant-design/icons';
import type { ActionType } from '@ant-design/pro-components';
import { DragSortTable, EditableProTable, ProCard, ProTable } from '@ant-design/pro-components';
import { Button, Popconfirm, Space, Table } from 'antd';
import React, { useRef, useState } from 'react';
import UpdateDoc from './UpdateDoc';
const FormDesign: React.FC<{ modId: number }> = (props) => {
  const [updateSow, setUpdateShow] = useState<any>(0);
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);
  const [dataSource, setDataSource] = useState<any[]>([]);
  const [tableTypes, setTableTypes] = useState<any>(1);
  const [sortDataSource, setSortDatasource] = useState<any>([]);
  const leftRef = useRef<ActionType>();
  const rightRef = useRef<ActionType>();
  const leftcolumns: any[] = [
    {
      title: '字段标题',
      dataIndex: 'title',
      ellipsis: 'true',
    },
    {
      title: '字段名',
      dataIndex: 'name',
      search: false,
    },
    {
      title: '操作',
      width: 60,
      key: 'option',
      valueType: 'option',
      render: (_: any, record: any, _index: any, action: { reload: () => void }) => [
        <a
          key="formattradd"
          onClick={async () => {
            await adminApi.cms.AdminapiCmsFormattradd({
              attr_id: record.id,
              mod_id: props.modId,
              alias: record.title,
            });
            action?.reload();
            rightRef?.current?.reload();
          }}
        >
          添加
        </a>,
      ],
    },
  ];
  const rightcolumns: any[] = [
    {
      title: '标题别名',
      dataIndex: 'alias',
      tooltip: '表单展示的标题,如果不修改默认字段原标题',
      // formItemProps: (form, { rowIndex }) => {
      //   return {
      //     rules: rowIndex > 1 ? [{ required: true, message: '此项为必填项' }] : [],
      //   };
      // },
      // // 第一行不允许编辑
      // editable: (text, record, index) => {
      //   return index !== 0;
      // },
    },
    {
      title: '字段名',
      dataIndex: 'a_name',
      tooltip: '只读，使用form.getFieldValue可以获取到值',
      readonly: true,
    },
    {
      title: 'span',
      dataIndex: 'span',
      valueType: 'digit',
      tooltip: '栅格占位格数，为 0 时相当于 display: none',
    },
    {
      title: 'pull',
      dataIndex: 'pull',
      valueType: 'digit',
      tooltip: '栅格向左移动格数',
    },
    {
      title: 'push',
      dataIndex: 'push',
      valueType: 'digit',
      tooltip: '栅格向右移动格数',
    },
    {
      title: 'offset',
      dataIndex: 'offset',
      valueType: 'digit',
      tooltip: '栅格左侧的间隔格数，间隔内不可以有栅格',
    },
    {
      title: '操作',
      valueType: 'option',
      width: 120,
      fixed: 'right',
      render: (text: any, record: { id: any }, _: any, action: any) => [
        <a
          key="editable"
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
        >
          编辑
        </a>,
        <Popconfirm
          key="delete"
          title={`确认删除吗?`}
          okText="是"
          cancelText="否"
          onConfirm={async () => {
            await adminApi.cms.AdminapiCmsFormattrdel({
              id: record.id,
            });
            action?.reload();
            leftRef?.current?.reload();
          }}
        >
          <a>删除</a>
        </Popconfirm>,
      ],
    },
  ];
  const sortColumns: any[] = [
    {
      title: '排序',
      dataIndex: 'sort',
      width: 60,
    },
    {
      title: '标题别名',
      dataIndex: 'alias',
      tooltip: '表单展示的标题,如果不修改默认字段原标题',
    },
    {
      title: '字段名',
      dataIndex: 'a_name',
      tooltip: '只读，使用form.getFieldValue可以获取到值',
    },
    {
      title: 'span',
      dataIndex: 'span',
      valueType: 'digit',
      tooltip: '栅格占位格数，为 0 时相当于 display: none',
    },
    {
      title: 'pull',
      dataIndex: 'pull',
      valueType: 'digit',
      tooltip: '栅格向左移动格数',
    },
    {
      title: 'push',
      dataIndex: 'push',
      valueType: 'digit',
      tooltip: '栅格向右移动格数',
    },
    {
      title: 'offset',
      dataIndex: 'offset',
      valueType: 'digit',
      tooltip: '栅格左侧的间隔格数，间隔内不可以有栅格',
    },
  ];
  const handleDragSortEnd = async (newDataSource: any) => {
    // console.log('排序后的数据', newDataSource);
    setSortDatasource(newDataSource);
    await adminApi.cms.AdminapiCmsFormsort({}, newDataSource);
    // message.success('修改列表排序成功');
  };
  return (
    <>
      <ProCard split="vertical" gutter={16} bodyStyle={{ paddingLeft: 8, paddingRight: 8 }}>
        <ProCard ghost>
          <ProTable
            ghost
            actionRef={leftRef}
            columns={leftcolumns}
            rowSelection={{
              // 自定义选择项参考: https://ant.design/components/table-cn/#components-table-demo-row-selection-custom
              // 注释该行则默认不显示下拉选项
              selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
              defaultSelectedRowKeys: [],
            }}
            tableAlertRender={({ selectedRowKeys, onCleanSelected }) => (
              <Space size={24}>
                <span>
                  已选 {selectedRowKeys.length} 项
                  <a style={{ marginLeft: 8 }} onClick={onCleanSelected}>
                    取消选择
                  </a>
                </span>
              </Space>
            )}
            tableAlertOptionRender={() => {
              return (
                <Space size={16}>
                  <a>批量添加</a>
                </Space>
              );
            }}
            request={async (params: any = {}, sort, filter): Promise<any> => {
              console.log(sort, filter);
              return await adminApi.cms.AdminapiCmsFormattrlist(params);
            }}
            params={{ mod_id: props.modId }}
            options={false}
            search={false}
            rowKey="id"
            headerTitle="可用字段"
            pagination={false}
            toolBarRender={() => [
              <Button
                key="shuaxin"
                type="dashed"
                shape="circle"
                icon={<ReloadOutlined />}
                onClick={() => leftRef?.current?.reload()}
              />,
            ]}
          />
        </ProCard>
        <ProCard colSpan={17} ghost>
          {tableTypes === 1 ? (
            <EditableProTable
              ghost
              actionRef={rightRef}
              rowKey="id"
              headerTitle="编辑表单"
              scroll={{
                x: 960,
              }}
              recordCreatorProps={false}
              loading={false}
              columns={rightcolumns}
              request={async (params: any): Promise<any> => {
                const res = await adminApi.cms.AdminapiCmsFormattrselect(params);
                setSortDatasource(res.data);
                return res;
              }}
              params={{ mod_id: props.modId }}
              value={dataSource}
              onChange={setDataSource}
              toolBarRender={() => [
                <Button
                  key="sortarrt"
                  icon={<SortAscendingOutlined />}
                  onClick={() => setTableTypes(2)}
                >
                  字段排序
                </Button>,
                <Button key="show" icon={<EyeOutlined />} onClick={() => setUpdateShow(1)}>
                  预览表单
                </Button>,
              ]}
              editable={{
                type: 'multiple',
                editableKeys,
                onSave: async (rowKey, data, row) => {
                  console.log(rowKey, data, row);
                  await adminApi.cms.AdminapiCmsFormattredit({}, data);
                  rightRef?.current?.reload();
                },
                onChange: setEditableRowKeys,
              }}
            />
          ) : (
            <DragSortTable
              ghost
              headerTitle="表单排序"
              columns={sortColumns}
              rowKey="id"
              pagination={false}
              dataSource={sortDataSource}
              dragSortKey="sort"
              onDragSortEnd={handleDragSortEnd}
              search={false}
              options={false}
              toolBarRender={() => [
                <Button key="editarrt" icon={<EditOutlined />} onClick={() => setTableTypes(1)}>
                  编辑字段
                </Button>,
                <Button key="show" icon={<EyeOutlined />} onClick={() => setUpdateShow(1)}>
                  预览表单
                </Button>,
              ]}
            />
          )}
        </ProCard>
      </ProCard>
      <UpdateDoc
        onCancel={(v): void => {
          if (!v) {
            setUpdateShow(0);
          }
        }}
        onSubmit={function (values: any): void {
          console.log(values);
        }}
        updateShow={updateSow}
        values={{}}
        modId={props.modId}
      />
    </>
  );
};

export default FormDesign;
