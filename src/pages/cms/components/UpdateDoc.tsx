import adminApi from '@/services/adminApi';
import type { ProFormInstance } from '@ant-design/pro-components';
import { BetaSchemaForm, ProProvider } from '@ant-design/pro-components';
import { Input } from 'antd';
import React, { useContext, useEffect, useRef, useState } from 'react';
import UploadPic from './valueType/UploadPic';
import Wangeditor from './valueType/Wangeditor';
const UpdateDoc: React.FC<{
  onCancel: (v: any) => void;
  onSubmit: (values: any) => void;
  updateShow: number;
  values: any;
  modId: number;
}> = (props) => {
  const values = useContext(ProProvider);
  const formRef = useRef<ProFormInstance>();
  const [columns, setColumns] = useState<any>([]);
  const getSchema = async () => {
    const res = await adminApi.cms.AdminapiCmsFormschema({ mod_id: props.modId });
    setColumns(res.data);
  };
  useEffect(() => {
    if (props.updateShow > 0) {
      getSchema();
    }
  }, [props.updateShow]);
  return (
    <ProProvider.Provider
      value={{
        ...values,
        valueTypeMap: {
          link: {
            render: (text) => <a>{text}</a>,
            renderFormItem: (text, _props) => (
              <Input placeholder="请输入链接" {..._props?.fieldProps} />
            ),
          },
          uploadPic: {
            render: (text) => {
              return text;
            },
            renderFormItem: (text, _props) => <UploadPic {..._props} {..._props?.fieldProps} />,
          },
          wangeditor: {
            render: (text) => {
              return text;
            },
            renderFormItem: (text, _props) => <Wangeditor {..._props} {..._props?.fieldProps} />,
          },
        },
      }}
    >
      <BetaSchemaForm
        initialValues={{ wangeditor: 'fdsafa' }}
        shouldUpdate={false}
        layoutType="DrawerForm"
        formRef={formRef}
        title={props.updateShow === 1 ? '新建内容' : '编辑内容'}
        visible={props.updateShow != 0}
        steps={[
          {
            title: 'ProComponent',
          },
        ]}
        rowProps={{
          gutter: [16, 0],
        }}
        grid={true}
        onFinish={async (value) => {
          console.log(value);
          props.onSubmit(value);
        }}
        columns={columns}
        onVisibleChange={(v) => {
          console.log(v);
          props.onCancel(v);
        }}
        drawerProps={{
          width: '90%',
          maskClosable: false,
          destroyOnClose: true,
        }}
      />
    </ProProvider.Provider>
  );
};
export default UpdateDoc;
