const fieldOptions = [
  { value: `text`, label: `文本框`, initialValue: '123456', type: 'STRING' },
  { value: `digit`, label: `数字输入框`, initialValue: '200000', type: 'INTEGER' },
  { value: `select`, label: `下拉框`, initialValue: 'open', type: 'INTEGER' },
  { value: `textarea`, label: `文本域`, initialValue: '123456\n121212', type: 'STRING' },
  { value: `date`, label: `日期`, initialValue: Date.now(), type: 'DATE' },
  { value: `dateTime`, label: `日期时间`, initialValue: Date.now(), type: 'DATE' },
  { value: `money`, label: `金额输入`, initialValue: '123456', type: 'DECIMAL(10, 2)' },
  { value: `checkbox`, label: `多选框`, initialValue: 'open', type: 'STRING' },
  { value: `rate`, label: `星级组件`, initialValue: '', type: 'FLOAT(1, 1)' },
  { value: `radio`, label: `单选框`, initialValue: 'open', type: 'INTEGER' },
  { value: `switch`, label: `开关`, initialValue: 'open', type: 'BOOLEAN' },
  { value: `uploadPic`, label: `单图上传`, initialValue: '', type: 'INTEGER' },
  { value: `wangeditor`, label: `富文本编辑器`, initialValue: '', type: 'TEXT' },
];
const extraFields = ['select', 'radio', 'checkbox'];
export default { fieldOptions: fieldOptions, extraFields: extraFields };
