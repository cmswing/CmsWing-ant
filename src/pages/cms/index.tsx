import { history } from '@umijs/max';
import React, { useEffect } from 'react';
const Index: React.FC = () => {
  useEffect(() => {
    history.push('/cms/doc');
  }, []);
  return <></>;
};
export default Index;
