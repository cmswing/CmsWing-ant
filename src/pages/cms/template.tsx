import { EllipsisOutlined, MailOutlined } from '@ant-design/icons';
import { PageContainer, ProCard } from '@ant-design/pro-components';
import { html } from '@codemirror/lang-html';
import { color } from '@uiw/codemirror-extensions-color';
import * as events from '@uiw/codemirror-extensions-events';
import CodeMirror from '@uiw/react-codemirror';
import { Button, Dropdown, Menu, Space } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
const Template: React.FC = () => {
  const [editorState, setEditorState] = useState<any>();
  const [editorView, setEditorView] = useState<any>();
  const [statistics, setStatistics] = useState<any>();
  const onChange = useCallback((value) => {
    console.log('value:', value);
  }, []);
  const eventExt = events.content({
    focus: (evn) => {
      console.log('focus', evn);
    },
    blur: () => {
      console.log('blur');
    },
  });

  useEffect(() => {
    console.log(editorState);
  }, [editorState]);
  const charu = useCallback(() => {
    const str = `{%set user = "{
      where: {
        rank: {
          [Op.or]: {
            [Op.lt]: 1000,
            [Op.eq]: null
          }
        },
        {
          createdAt: {
            [Op.lt]: new Date(),
            [Op.gt]: new Date(new Date() - 24 * 60 * 60 * 1000)
          }
        },
        {
          [Op.or]: [
            {
              title: {
                [Op.like]: 'Boat%'
              }
            },
            {
              description: {
                [Op.like]: '%boat%'
              }
            }
          ]
        }
      }
    }"|@cms_list %}`;
    editorView.dispatch({
      changes: { from: statistics.to, insert: str },
      selection: { anchor: statistics.to + str.length },
    });
  }, [editorView, statistics]);

  return (
    <PageContainer
      header={{
        title: false,
      }}
      tabBarExtraContent={
        <Space>
          <Button key="w1">次要按钮</Button>
          <Button key="w2">次要按钮</Button>
          <Button key="w3" type="primary" onClick={charu}>
            主要按钮
          </Button>
          <Dropdown
            key="dropdown"
            trigger={['click']}
            overlay={
              <Menu
                items={[
                  {
                    label: '下拉菜单',
                    key: '1',
                  },
                  {
                    label: '下拉菜单2',
                    key: '2',
                  },
                  {
                    label: '下拉菜单3',
                    key: '3',
                  },
                ]}
              />
            }
          >
            <Button key="4" style={{ padding: '0 8px' }}>
              <EllipsisOutlined />
            </Button>
          </Dropdown>
        </Space>
      }
      tabList={[
        {
          tab: '首页模版',
          key: 'home1',
          closable: false,
        },
        {
          tab: '封面模版',
          key: 'channel2',
          closable: false,
        },
        {
          tab: '列表模版',
          key: 'list3',
          closable: false,
        },
        {
          tab: '内容模版',
          key: 'detail4',
          closable: false,
        },
        {
          tab: '公共模版',
          key: 'gongg1',
          closable: false,
        },
      ]}
      tabProps={{
        type: 'editable-card',
        hideAdd: true,
        onEdit: (e, action) => console.log(e, action),
      }}
    >
      <ProCard split="vertical" ghost>
        <ProCard colSpan="200px" ghost>
          <Menu
            // onSelect={(e) => setKey(e.key as string)}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
            items={[
              {
                key: 'sub1',
                label: (
                  <span>
                    <MailOutlined />
                    <span>Navigation One</span>
                  </span>
                ),
                children: [
                  {
                    type: 'group',
                    key: 'g1',
                    label: 'Item 1',
                    children: [
                      {
                        key: '1',
                        label: 'Option 1',
                      },
                      {
                        key: '2',
                        label: 'Option 2',
                      },
                    ],
                  },
                  {
                    type: 'group',
                    key: 'g2',
                    label: 'Item 2',
                    children: [
                      {
                        key: '3',
                        label: 'Option 3',
                      },
                      {
                        key: '4',
                        label: 'Option 4',
                      },
                    ],
                  },
                ],
              },
            ]}
          />
        </ProCard>
        <ProCard title="模版编辑" bodyStyle={{ padding: 1 }} headerBordered>
          <CodeMirror
            value={`<div>
        {{ content }}
      {% if extra_content %}
        <hr></div><div class=extra>{{ extra_content }}
      {% endif %}
        <hr>
      </div>`}
            height="70vh"
            maxHeight="100vh"
            autoFocus={true}
            extensions={[html(), color, eventExt]}
            onChange={onChange}
            onStatistics={(sta) => {
              console.log(sta.selection.ranges[0]);
              setStatistics(sta.selection.ranges[0]);
            }}
            onCreateEditor={(view, state) => {
              setEditorState(state);
              setEditorView(view);
              // console.log(view.update());
              // console.log(state.replaceSelection('fdsa'));
            }}
          />
        </ProCard>
      </ProCard>
    </PageContainer>
  );
};
export default Template;
