import adminApi from '@/services/adminApi';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { PageContainer, ProCard, ProDescriptions } from '@ant-design/pro-components';
import { Button, message } from 'antd';
import React, { useEffect, useState } from 'react';
import FieldList from './components/FieldList';
import FormDesign from './components/FormDesign';
import TableDesign from './components/TableDesign';
import UpdateMod from './components/updateMod';
const Mod: React.FC = () => {
  const [modId, setModId] = useState(0);
  const [modInfo, setModInfo] = useState<any>({});
  const [modList, setModList] = useState<any[]>([]);
  const [tabKey, setTabKey] = useState<any>('1');
  const [modUpdateShow, setModUpdateShow] = useState<any>(0);
  const getMod = async () => {
    // 获取模型信息
    const res: { data: any } = await adminApi.cms.AdminapiCmsModlist({});
    const data = res?.data?.map((o: any) => {
      o.tab = o.title;
      o.key = o.id;
      o.closable = false;
      return o;
    });
    setModList(data);
    setModId(data[0].id);
    setModInfo(data[0]);
  };
  useEffect(() => {
    getMod();
  }, []);
  return (
    <PageContainer
      header={{
        ghost: true,
        title: modInfo.title,
        extra: [
          <Button key="2" danger icon={<DeleteOutlined />}>
            删除模型
          </Button>,
          <Button
            key="editMod"
            icon={<EditOutlined />}
            onClick={() => {
              setModUpdateShow(2);
            }}
          >
            编辑模型
          </Button>,
          <Button
            key="addMod"
            type="primary"
            icon={<PlusOutlined />}
            onClick={() => setModUpdateShow(1)}
          >
            添加模型
          </Button>,
        ],
      }}
      tabList={modList}
      tabActiveKey={modId.toString()}
      onTabChange={(key: any) => {
        console.log(key);
        setModId(key);
        const data = modList.filter((o) => o.id == key);
        setModInfo(data[0]);
      }}
      tabProps={{
        type: 'editable-card',
        hideAdd: true,
        onEdit: (e, action) => console.log(e, action),
      }}
    >
      <ProCard direction="column" ghost gutter={[0, 16]}>
        <ProCard>
          <ProDescriptions column={4}>
            <ProDescriptions.Item label="模型名称">{modInfo.title}</ProDescriptions.Item>
            <ProDescriptions.Item label="模型标识">{modInfo.name}</ProDescriptions.Item>
            <ProDescriptions.Item
              label="状态"
              valueEnum={{
                false: {
                  text: '禁用',
                  status: 'Error',
                },
                true: {
                  text: '启用',
                  status: 'Success',
                },
              }}
            >
              {modInfo.status}
            </ProDescriptions.Item>
            <ProDescriptions.Item label="日期时间" valueType="dateTime">
              {modInfo.createdAt}
            </ProDescriptions.Item>
            <ProDescriptions.Item label="优先级">{modInfo.level}</ProDescriptions.Item>
            <ProDescriptions.Item label="继承模型">
              {modInfo.extend !== 0 ? '基础模型' : '系统模型'}
            </ProDescriptions.Item>
            <ProDescriptions.Item
              span={2}
              valueType="text"
              contentStyle={{
                maxWidth: '80%',
              }}
              ellipsis
              label="模块描述"
            >
              {modInfo.remark}
            </ProDescriptions.Item>
          </ProDescriptions>
        </ProCard>
        <ProCard
          tabs={{
            activeKey: tabKey,
            onChange: (activeKey) => {
              console.log(activeKey);
              setTabKey(activeKey);
            },
          }}
          ghost
        >
          <ProCard.TabPane key="1" tab="字段管理" cardProps={{ ghost: true }}>
            {modId && tabKey == '1' && <FieldList modId={modId} />}
          </ProCard.TabPane>
          <ProCard.TabPane key="2" tab="表单设计" cardProps={{ ghost: true }}>
            {modId && tabKey == '2' && <FormDesign modId={modId} />}
          </ProCard.TabPane>
          <ProCard.TabPane key="3" tab="列表设计" cardProps={{ ghost: true }}>
            {modId && tabKey == '3' && <TableDesign modId={modId} />}
          </ProCard.TabPane>
        </ProCard>
      </ProCard>
      <UpdateMod
        onCancel={(): void => {
          setModUpdateShow(0);
        }}
        onSubmit={async (values: API.modItem): Promise<void> => {
          console.log(values);
          let res: any = {};
          if (modUpdateShow == 1) {
            res = await adminApi.cms.AdminapiCmsModadd({}, values);
          } else {
            res = await adminApi.cms.AdminapiCmsModedit({}, values);
          }
          if (res.errorCode === '0') {
            setModUpdateShow(0);
            message.success('操作成功');
            getMod();
          }
        }}
        visible={modUpdateShow}
        values={modUpdateShow == 2 ? modInfo : {}}
      />
    </PageContainer>
  );
};
export default Mod;
