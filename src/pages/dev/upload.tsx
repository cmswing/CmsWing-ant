import adminApi from '@/services/adminApi';
import { SettingOutlined } from '@ant-design/icons';
import { CheckCard, PageContainer, ProCard, ProDescriptions } from '@ant-design/pro-components';
import { Button, message, Space, Tag } from 'antd';
import React, { useEffect, useState } from 'react';
import UploadEdit from './components/UploadEdit';

const Upload: React.FC = () => {
  const [uploadtype, setUploadtype] = useState<any>('');
  const [dataSource, setDataSource] = useState<any>({});
  const [updateShow, setUpdateShow] = useState<any>(0);
  const getconfig = async () => {
    const res: any = await adminApi.cwDev.AdminapiCwDevUploadconfig({});
    console.log(res.data);
    setUploadtype(res.data.type);
    setDataSource(res.data);
  };
  const uploadedit = async (type: any) => {
    const ty: any = { type };
    await adminApi.cwDev.AdminapiCwDevUploadedit({}, { data: ty });
    message.success('修改成功');
  };
  useEffect(() => {
    getconfig();
  }, []);
  return (
    <PageContainer>
      <ProCard title="上传配置" extra="在生产环节修改配置后要重启服务器" tooltip="这是提示">
        <CheckCard.Group
          // options={dataSource}
          value={uploadtype}
          onChange={(checkedValue) => {
            console.log(checkedValue);
            if (checkedValue) {
              setUploadtype(checkedValue);
              uploadedit(checkedValue);
            }
          }}
        >
          <CheckCard
            avatar="/public/admin/logo.svg"
            title={
              <>
                <Space>
                  本地存储
                  <Tag color="blue">local</Tag>
                </Space>
              </>
            }
            description={<Space style={{ width: 230 }}>{dataSource?.local?.domain}</Space>}
            value="local"
            extra={
              <Button
                type="primary"
                icon={<SettingOutlined />}
                size="small"
                onClick={(e) => {
                  e.stopPropagation();
                  setUpdateShow('local');
                }}
              >
                设置
              </Button>
            }
          />
          <CheckCard
            avatar="/public/admin/qiniu.png"
            title={
              <>
                <Space>
                  七牛
                  <Tag color="blue">qiniu</Tag>
                </Space>
              </>
            }
            description={<Space style={{ width: 230 }}>{dataSource?.qiniu?.domain}</Space>}
            value="qiniu"
            extra={
              <Button
                type="primary"
                icon={<SettingOutlined />}
                size="small"
                onClick={(e) => {
                  e.stopPropagation();
                  setUpdateShow('qiniu');
                }}
              >
                设置
              </Button>
            }
          />
          <CheckCard
            avatar="/public/admin/huaweiyun.svg"
            title={
              <>
                <Space>
                  华为云
                  <Tag color="blue">huawei</Tag>
                </Space>
              </>
            }
            description={<Space style={{ width: 230 }}>{dataSource?.huawei?.domain}</Space>}
            value="huawei"
            extra={
              <Button
                type="primary"
                icon={<SettingOutlined />}
                size="small"
                onClick={(e) => {
                  e.stopPropagation();
                  setUpdateShow('huawei');
                }}
              >
                设置
              </Button>
            }
          />
        </CheckCard.Group>
        <ProDescriptions column={6} title="上传接口文档" tooltip="接口文档">
          <ProDescriptions.Item span={2} valueType="text" copyable label="接口地址">
            /adminApi/cw_dev/upload
          </ProDescriptions.Item>
          <ProDescriptions.Item span={1} label="请求类型" valueType="text">
            POST
          </ProDescriptions.Item>
          <ProDescriptions.Item span={3} label="name" valueType="text">
            file
          </ProDescriptions.Item>

          <ProDescriptions.Item span={6} label="权限说明" valueType="textarea">
            请求接口时要在 form data 里面携带 token,这个token是cmswing里面自带的jwt方法生成的。
          </ProDescriptions.Item>
          <ProDescriptions.Item span={6} label="formData" valueType="textarea">
            请求中其他额外的 form data,具体参数参考下面的列表
          </ProDescriptions.Item>
          <ProDescriptions.Item span={1} label="参数" valueType="text" copyable>
            token
          </ProDescriptions.Item>
          <ProDescriptions.Item span={1} label="必须" valueType="text">
            是
          </ProDescriptions.Item>
          <ProDescriptions.Item span={4} label="说明" valueType="textarea">
            cmswing里面自带的jwt方法生成的token
          </ProDescriptions.Item>
          <ProDescriptions.Item span={1} label="参数" valueType="text" copyable>
            type
          </ProDescriptions.Item>
          <ProDescriptions.Item span={1} label="必须" valueType="text">
            否
          </ProDescriptions.Item>
          <ProDescriptions.Item span={4} label="说明" valueType="textarea">
            如果formData携带
            type,就会强制使用type指定的储存类型，如果不携带type则使用默认的存储类型。
          </ProDescriptions.Item>
          <ProDescriptions.Item span={1} label="参数" valueType="text" copyable>
            resBody
          </ProDescriptions.Item>
          <ProDescriptions.Item span={1} label="必须" valueType="text">
            否
          </ProDescriptions.Item>
          <ProDescriptions.Item span={4} label="说明" valueType="textarea">
            自定义返回格式 比如:{'{"id":{{id}},"path":"{{url}}"}'},具体支持参数参考{' '}
            <b>resBody参数</b>
          </ProDescriptions.Item>
          <ProDescriptions.Item span={6} label="resBody参数" valueType="code">
            {`//说明: number 类型的 定义格式时直接输入{{}}, string 类型的定义格式时要加“”,比如'{"id":{{id}},"size":{{size}},"url":"{{url}}"}'
{{id}}<number> //文件id
{{name}}<string> // 原始文件名
{{mime}}<string> // 文件mime类型
{{size}}<number> // 文件大小kb
{{type}}<string> // 文件保存位置
{{savename}}<string> // 保存名称
{{hash}}<string> // hash
{{url}}<string> // 文件地址
//如果请求时不携带 resBody ，默认返回 '{
                                    "id":{{id}},
                                    "name":"{{name}}",
                                    "mime":"{{mime}}",
                                    "size":{{size}},
                                    "type":"{{type}}",
                                    "savename":"{{savename}}",
                                    "hash":"{{hash}}",
                                    "url":"{{url}}"
                                   }'
           `}
          </ProDescriptions.Item>

          <ProDescriptions.Item span={6} label="接口默认返回格式" valueType="jsonCode">
            {`
{
  id: 9,
  name: '123 (1).png',
  mime: 'image/png',
  size: 11,
  type: 'qiniu',
  savename: '465ba8ef-8ea6-4d85-8c4c-dcda32f7f633.png',
  hash: 'FlS-VJYTDmH56gP0MJUIvvHQgsPS',
  url: 'https://data.cmswing.com/465ba8ef-8ea6-4d85-8c4c-dcda32f7f633.png',
}
          `}
          </ProDescriptions.Item>
          <ProDescriptions.Item span={3} label="ant上传组件例子" valueType="code">
            {` <Upload
      name="file"
      listType="picture-card"
      className="avatar-uploader"
      showUploadList={false}
      action={\`\${API_URL}/adminApi/cw_dev/upload\`}
      data={{
        token: localStorage.getItem('token') || '',
        resBody: '{"id":{{id}},"path":"{{url}}"}',
      }}
      beforeUpload={beforeUpload}
      onChange={handleChange}
    >
`}
          </ProDescriptions.Item>
          <ProDescriptions.Item span={3} label="小程序上传组件例子" valueType="code">
            {`wx.chooseImage({
  success (res) {
    const tempFilePaths = res.tempFilePaths
    wx.uploadFile({
      //仅为示例，非真实的接口地址example.com是你自己的域名
      url: 'https://example.com/adminApi/cw_dev/upload',
      filePath: tempFilePaths[0],
      name: 'file',
      formData: {
        token: wx.getStorageSync('token') || '',
        resBody: '{"id":{{id}},"path":"{{url}}"}',
      },
      success (res){
        const data = res.data
        //do something
      }
    })
  }
})
`}
          </ProDescriptions.Item>
        </ProDescriptions>
      </ProCard>
      <UploadEdit
        onCancel={(v) => {
          console.log(v);
          if (!v) {
            setUpdateShow(0);
          }
        }}
        onSubmit={async (values: any): Promise<void> => {
          await adminApi.cwDev.AdminapiCwDevUploadedit({}, { data: values });
          message.success('修改成功');
          setUpdateShow(0);
          await getconfig();
        }}
        updateShow={updateShow}
      />
    </PageContainer>
  );
};
export default Upload;
