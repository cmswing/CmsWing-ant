import adminApi from '@/services/adminApi';
import type { ProFormInstance } from '@ant-design/pro-components';
import { BetaSchemaForm } from '@ant-design/pro-components';
import React, { useEffect, useRef, useState } from 'react';
const UploadEdit: React.FC<{
  onCancel: (v: any) => void;
  onSubmit: (values: any) => void;
  updateShow: any;
}> = (props) => {
  console.log(props);
  const formRef = useRef<ProFormInstance>();
  const [columns, setColumns] = useState<any>([]);
  const getSchema = async () => {
    const res = await adminApi.cwDev.AdminapiCwDevUploadschema({ type: props.updateShow });
    setColumns(res.data);
  };
  useEffect(() => {
    if (props.updateShow != 0) {
      getSchema();
    }
  }, [props.updateShow]);
  return (
    <BetaSchemaForm
      initialValues={{ wangeditor: 'fdsafa' }}
      shouldUpdate={false}
      layoutType="ModalForm"
      formRef={formRef}
      title={`编辑${props.updateShow}`}
      visible={props.updateShow !== 0}
      steps={[
        {
          title: 'ProComponent',
        },
      ]}
      rowProps={{
        gutter: [16, 0],
      }}
      grid={true}
      onFinish={async (value) => {
        console.log(value);
        props.onSubmit(value);
      }}
      columns={columns}
      onVisibleChange={(v) => {
        console.log(v);
        props.onCancel(v);
      }}
      modalProps={{
        maskClosable: false,
        destroyOnClose: true,
      }}
    />
  );
};
export default UploadEdit;
