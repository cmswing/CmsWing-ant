import iconfontApi from '@/services';
import adminApi from '@/services/adminApi';
import {
  AppstoreAddOutlined,
  createFromIconfontCN,
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import {
  ModalForm,
  PageContainer,
  ProForm,
  ProFormDigit,
  ProFormRadio,
  ProFormSelect,
  ProFormSwitch,
  ProFormText,
  ProFormTreeSelect,
  ProTable,
} from '@ant-design/pro-components';
import { Badge, Button, Card, Descriptions, Menu, message, Popconfirm, Space } from 'antd';
import { useEffect, useRef, useState } from 'react';
const IconFont = createFromIconfontCN({
  scriptUrl: '/public/admin/iconfont/iconfont.js',
});
// const waitTime = (time: number = 100) => {
//   return new Promise((resolve) => {
//     setTimeout(() => {
//       resolve(true);
//     }, time);
//   });
// };

export type TableListItem = {
  key: number;
  name: string;
  createdAt: number;
  progress: number;
};

export default () => {
  const [mod_id, setMod_id] = useState<any>('1');
  const [modalVisit, setModalVisit] = useState(false);
  const [editModalVisit, setEditModalVisit] = useState(false);
  const [addRoutesVisit, setAddRoutesVisit] = useState(false);
  const [modInfo, setModInfo] = useState<any>({});
  const [modList, setModList] = useState<any[]>([]);
  const [delBtnVisible, setDelBtnVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [admin, setAdmin] = useState(true);
  const [action, setAction] = useState<any>([]);
  const [actonSelect, setActonSelect] = useState('');
  const [verb, setVerb] = useState('get');
  const [editRoutesVisit, setEditRoutesVisit] = useState(false);
  const [editRoutes, setEditRoutes] = useState<any>({});
  const actionRef = useRef<ActionType>();
  const columns: ProColumns[] = [
    {
      title: '排序',
      dataIndex: 'sort',
      key: 'sort',
      width: 100,
      search: false,
      tooltip: '数值越小越靠前',
      fixed: 'left',
    },
    {
      title: '路由名称',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      tooltip: '路由名称',
      fixed: 'left',
      ellipsis: true,
    },
    {
      title: 'rid',
      dataIndex: 'id',
      key: 'id',
      width: 80,
      tooltip: '节点id:当前端要手动判断节点权限时用rid来识别',
      fixed: 'left',
      copyable: true,
    },
    {
      title: '图标',
      dataIndex: 'icon',
      tooltip: '只对前端有效',
      search: false,
      width: 80,
      render: (dom: any) => (
        <Space>
          <span>{dom != '-' ? <IconFont type={dom} /> : '-'}</span>
        </Space>
      ),
    },
    {
      title: 'URL路径',
      dataIndex: 'path',
      key: 'path',
      tooltip: 'URL路径',
      copyable: true,
      ellipsis: true,
    },
    {
      title: '控制器',
      dataIndex: 'controller',
      tooltip: 'controller:egg的controller',
      ellipsis: true,
    },
    {
      title: '方法',
      dataIndex: 'action',
      tooltip: '控制器的方法',
      ellipsis: true,
    },
    {
      title: '类型',
      tooltip: '后端egg路由,前端ant路由',
      dataIndex: 'admin',
      width: 80,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '后端',
        },
        false: {
          text: '前端',
        },
      },
    },
    {
      title: '权限节点',
      tooltip: '如果是权限节点会在角色里面显示节点',
      dataIndex: 'role',
      width: 110,
      valueType: 'select',
      search: false,
      valueEnum: {
        true: {
          text: '是',
        },
        false: {
          text: '否',
        },
      },
    },
    {
      title: '触发动作',
      tooltip: 'verb:用户触发动作，支持 get，post 等所有 HTTP 方法,针对后端',
      dataIndex: 'verb',
      width: 110,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        get: {
          text: 'get',
        },
        post: {
          text: 'post',
        },
        put: {
          text: 'put',
        },
        del: {
          text: 'del',
        },
        redirect: {
          text: 'redirect',
        },
        resources: {
          text: 'resources',
        },
        patch: {
          text: 'patch',
        },
        head: {
          text: 'head',
        },
        options: {
          text: 'options',
        },
      },
    },
    {
      title: '中间件',
      dataIndex: 'middleware',
      tooltip: 'middleware:只对后端有效',
      ellipsis: true,
    },
    {
      title: '排除中间件',
      dataIndex: 'ignoreMiddleware',
      tooltip: 'ignoreMiddleware:排除模块统一设置的中间件',
      ellipsis: true,
    },
    {
      title: 'parentKeys',
      dataIndex: 'parentKeys',
      tooltip: 'parentKeys:当此节点被选中的时候也会选中 parentKeys 的节点，前端有效',
      ellipsis: true,
    },
    {
      title: '权限',
      dataIndex: 'access',
      tooltip: 'access:权限配置，需要预先配置权限，前端有效',
      ellipsis: true,
    },
    {
      title: '路由',
      tooltip: 'hideInMenu:在菜单中不展示这个路由，包括子路由',
      dataIndex: 'hideInMenu',
      search: false,
      width: 80,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '隐藏',
        },
        false: {
          text: '显示',
        },
      },
    },
    {
      title: '子路由',
      tooltip: 'hideChildrenInMenu:用于隐藏不需要在菜单中展示的子路由,前端有效',
      dataIndex: 'hideChildrenInMenu',
      search: false,
      width: 110,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '隐藏',
        },
        false: {
          text: '显示',
        },
      },
    },
    {
      title: '面包屑',
      tooltip: 'hideInBreadcrumb:隐藏不需要在菜单中展示的子路由,前端有效',
      dataIndex: 'hideInBreadcrumb',
      valueType: 'select',
      search: false,
      width: 110,
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '隐藏',
        },
        false: {
          text: '显示',
        },
      },
    },
    {
      title: '顶栏',
      tooltip: 'headerRender:当前路由不展示顶栏,前端有效',
      dataIndex: 'headerRender',
      search: false,
      width: 80,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '显示',
        },
        false: {
          text: '隐藏',
        },
      },
    },
    {
      title: '页脚',
      tooltip: 'footerRender:当前路由展示页脚,前端有效',
      dataIndex: 'footerRender',
      search: false,
      width: 80,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '显示',
        },
        false: {
          text: '隐藏',
        },
      },
    },
    {
      title: '菜单',
      tooltip: 'menuRender:当前路由展示菜单,前端有效',
      dataIndex: 'menuRender',
      search: false,
      width: 80,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '显示',
        },
        false: {
          text: '隐藏',
        },
      },
    },
    {
      title: '菜单顶栏',
      tooltip: 'menuHeaderRender:当前路由不展示菜单顶栏,前端有效',
      dataIndex: 'menuHeaderRender',
      search: false,
      width: 120,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '显示',
        },
        false: {
          text: '隐藏',
        },
      },
    },
    {
      title: '子项上提',
      tooltip: 'flatMenu:子项往上提，只是不展示父菜单,前端有效',
      dataIndex: 'flatMenu',
      search: false,
      width: 120,
      valueType: 'select',
      valueEnum: {
        all: { text: '全部' },
        true: {
          text: '是',
        },
        false: {
          text: '否',
        },
      },
    },
    {
      title: '操作',
      dataIndex: 'x',
      valueType: 'option',
      render: (_, record) => {
        const node = (
          <Popconfirm
            key={record.id}
            title={`确认删除吗?`}
            okText="是"
            cancelText="否"
            onConfirm={async () => {
              const data: any = await adminApi.cwDev.AdminapiCwDevDelrouter({ id: record.id });
              if (data.errorCode == 0) {
                actionRef?.current?.reload();
                message.success('删除成功');
              }
              return;
            }}
          >
            <a>删除</a>
          </Popconfirm>
        );
        return [
          <a
            key="edit"
            onClick={async () => {
              console.log(record);
              setAdmin(record.admin);
              setVerb(record.verb);
              const data = await adminApi.cwDev.AdminapiCwDevGetcontrollerfun({
                c: record.controller,
              });
              setAction(data.data);
              setActonSelect(record.action);
              setEditRoutes(record);
              setEditRoutesVisit(true);
            }}
          >
            编辑
          </a>,
          node,
        ];
      },
      fixed: 'right',
      width: 120,
    },
  ];
  //获取ico
  const getIconFont = async () => {
    const res = await iconfontApi.iconfont();
    const pre = res.css_prefix_text;
    for (const v of res.glyphs) {
      v.value = `${pre}${v.font_class}`;
      v.label = `${v.name}`;
    }
    return res.glyphs;
  };
  //获取详情
  const getModInfo = async (id?: any) => {
    const res = await adminApi.cwDev.AdminapiCwDevGetmodinfo(id); //getModInfo
    setMod_id(res?.data?.id?.toString());
    setModInfo(res.data);
  };
  //获取模块列表
  const getModList = async () => {
    const res = await adminApi.cwDev.AdminapiCwDevGetmodlist({}); //getModList
    setModList(res.data);
  };
  useEffect(() => {
    // Update the document title using the browser API
    getModInfo();
    getModList();
  }, []);
  return (
    <PageContainer
      header={{
        extra: [
          <Button
            key="addMod"
            onClick={() => {
              setModalVisit(true);
            }}
            type="primary"
            icon={<AppstoreAddOutlined />}
          >
            添加模块
          </Button>,
        ],
      }}
    >
      <ProTable<TableListItem>
        search={{ labelWidth: 'auto' }}
        columns={columns}
        rowKey="id"
        pagination={false}
        /* eslint-disable-next-line @typescript-eslint/no-unused-vars */
        tableExtraRender={(_, data) => (
          <Card
            title={
              <Menu
                onSelect={async (e) => {
                  setMod_id(e.key as string);
                  await getModInfo({ id: e.key });
                }}
                defaultSelectedKeys={[mod_id]}
                mode="horizontal"
                items={modList}
                style={{ marginTop: '-26px', marginBottom: '-17px' }}
              />
            }
            headStyle={{ paddingLeft: 5 }}
            extra={
              <Space>
                {!modInfo?.sys && (
                  <Popconfirm
                    title="删除模块后会连同模块下的路由一起删除！"
                    visible={delBtnVisible}
                    onConfirm={async () => {
                      setConfirmLoading(true);
                      const res: any = await adminApi.cwDev.AdminapiCwDevDelmod({
                        id: modInfo.id,
                      }); //delMod
                      if (res.errorCode == 0) {
                        setDelBtnVisible(false);
                        setConfirmLoading(false);
                        await getModInfo();
                        await getModList();
                        message.success('提交成功');
                      }
                    }}
                    okButtonProps={{ loading: confirmLoading }}
                    onCancel={() => {
                      setDelBtnVisible(false);
                    }}
                  >
                    <Button
                      key="delMod"
                      type="primary"
                      onClick={() => {
                        setDelBtnVisible(true);
                      }}
                      danger
                      icon={<DeleteOutlined />}
                    >
                      删除模块
                    </Button>
                  </Popconfirm>
                )}
                <Button
                  key="editMod"
                  type="primary"
                  onClick={() => {
                    setEditModalVisit(true);
                  }}
                  icon={<EditOutlined />}
                >
                  编辑模块
                </Button>
                <Button
                  key="addRoutes"
                  type="primary"
                  onClick={() => {
                    setAddRoutesVisit(true);
                  }}
                  icon={<PlusOutlined />}
                >
                  添加路由
                </Button>
              </Space>
            }
          >
            <Descriptions size="small" column={4}>
              <Descriptions.Item label="模块名称">{modInfo?.name}</Descriptions.Item>
              <Descriptions.Item label="模块PATH">{modInfo?.path}</Descriptions.Item>
              <Descriptions.Item label="排序">{modInfo?.sort}</Descriptions.Item>
              <Descriptions.Item label="系统">
                <Badge status="processing" text={modInfo?.sys ? '是' : '否'} />
              </Descriptions.Item>
              <Descriptions.Item span={2} label="中间件">
                {modInfo?.middleware}
              </Descriptions.Item>
              <Descriptions.Item span={2} label="描述">
                {modInfo?.remarks}
              </Descriptions.Item>
            </Descriptions>
          </Card>
        )}
        params={{
          mod_id,
        }}
        request={async (params: any = {}, sort, filter): Promise<any> => {
          console.log(params, sort, filter);
          return await adminApi.cwDev.AdminapiCwDevGettreemenu(params); //getTreeMenu
        }}
        actionRef={actionRef}
        dateFormatter="string"
        headerTitle="路由"
        scroll={{ x: 3000 }}
      />

      <ModalForm
        title="新建模块"
        visible={modalVisit}
        onFinish={async (values: API.modRequest) => {
          console.log(values);
          const data = await adminApi.cwDev.AdminapiCwDevAddmod({}, values); //addMod
          if (data.errorCode == '0') {
            await getModInfo({ id: mod_id });
            await getModList();
            message.success('提交成功');
          }
          return true;
        }}
        onVisibleChange={setModalVisit}
        modalProps={{
          maskClosable: false,
          destroyOnClose: true,
        }}
      >
        <ProForm.Group>
          <ProFormText
            width="md"
            name="name"
            label="模块名称"
            tooltip="路由名称"
            placeholder="请输入路由名称"
            rules={[{ required: true }]}
          />
          <ProFormText
            width="md"
            name="path"
            label="模块PATH"
            tooltip="模块的PATH"
            placeholder="模块PATH"
            rules={[{ required: true }]}
          />
        </ProForm.Group>

        <ProFormSelect
          name="middleware"
          label="中间件"
          request={async () => {
            const data = await adminApi.cwDev.AdminapiCwDevGetmiddleware({}); //getMiddleware
            return data.data;
          }}
          tooltip="在 Router 里面可以配置多个 Middleware"
          placeholder="请选择Middleware"
          fieldProps={{ mode: 'multiple' }}
        />
        <ProFormText
          name="remarks"
          label="模块描述"
          tooltip="模块描述"
          placeholder="请输入模块描述"
        />
        <ProFormDigit
          width="xs"
          label="排序"
          name="sort"
          tooltip="数字越小越靠前"
          placeholder="0"
          fieldProps={{ precision: 0 }}
        />
      </ModalForm>
      <ModalForm
        title="编辑模块"
        visible={editModalVisit}
        onFinish={async (values: any) => {
          values.id = modInfo?.id;
          console.log(values);
          const data = await adminApi.cwDev.AdminapiCwDevEditmod({}, values); //editMod
          if (data.errorCode == '0') {
            await getModInfo({ id: modInfo?.id });
            await getModList();
            message.success('提交成功');
          }
          return true;
        }}
        onVisibleChange={setEditModalVisit}
        modalProps={{
          maskClosable: false,
          destroyOnClose: true,
        }}
      >
        <ProForm.Group>
          <ProFormText
            width="md"
            name="name"
            label="模块名称"
            tooltip="路由名称"
            placeholder="请输入路由名称"
            rules={[{ required: true }]}
            initialValue={modInfo?.name}
          />
          <ProFormText
            width="md"
            name="path"
            label="模块PATH"
            tooltip="模块的PATH"
            placeholder="模块PATH"
            rules={[{ required: true }]}
            initialValue={modInfo?.path}
          />
        </ProForm.Group>

        <ProFormSelect
          name="middleware"
          label="中间件"
          request={async () => {
            const data = await adminApi.cwDev.AdminapiCwDevGetmiddleware({}); //getMiddleware
            return data.data;
          }}
          tooltip="在 Router 里面可以配置多个 Middleware"
          placeholder="请选择Middleware"
          fieldProps={{ mode: 'multiple' }}
          initialValue={modInfo?.middlewareArr}
        />
        <ProFormText
          name="remarks"
          label="模块描述"
          tooltip="模块描述"
          placeholder="请输入模块描述"
          initialValue={modInfo?.remarks}
        />
        <ProFormDigit
          width="xs"
          label="排序"
          name="sort"
          tooltip="数字越小越靠前"
          placeholder="0"
          fieldProps={{ precision: 0 }}
          initialValue={modInfo?.sort}
        />
      </ModalForm>

      <ModalForm
        title="添加路由"
        visible={addRoutesVisit}
        onFinish={async (values) => {
          console.log(values);
          const data = await adminApi.cwDev.AdminapiCwDevAddrouter({}, values); //addRouter
          if (data.errorCode == '0') {
            actionRef?.current?.reload();
            message.success('提交成功');
          }
          return true;
        }}
        onVisibleChange={(visible) => {
          console.log(visible);
          setAddRoutesVisit(visible);
          if (!visible) {
            setAdmin(true);
            setVerb('get');
            setAction([]);
            setActonSelect('');
          }
        }}
        modalProps={{
          maskClosable: false,
          destroyOnClose: true,
        }}
      >
        <ProForm.Group>
          <ProFormText
            width="md"
            name="name"
            label="路由名称"
            tooltip="路由名称"
            placeholder="请输入路由名称"
            rules={[{ required: true }]}
          />
          <ProFormText
            width="md"
            name="path"
            label="路由URL路径"
            tooltip="路由URL路径"
            placeholder="请输入路由URL路径"
            rules={[{ required: true }]}
          />
          <ProFormSelect
            width="md"
            name="icon"
            label="路由图标"
            request={async () => {
              return await getIconFont();
            }}
            tooltip="使用 iconfont.cn"
            placeholder="请选择图标"
            fieldProps={{
              optionLabelProp: 'value',
              showSearch: true,
              optionItemRender(item) {
                return (
                  <>
                    <IconFont type={item.value} /> {item.label}
                  </>
                );
              },
            }}
          />
          <ProFormTreeSelect
            width="md"
            name="pid"
            label="上级路由"
            rules={[{ required: true }]}
            request={async (): Promise<any> => {
              const data = await adminApi.cwDev.AdminapiCwDevGetalltreemenu({
                mod_id: modInfo.id,
              }); //getAllTreeMenu
              return data.data;
            }}
            tooltip="如果是一级菜单请选择一级菜单"
            placeholder="请选择上级路由"
            fieldProps={{
              fieldNames: {
                label: 'name',
                value: 'id',
              },
              showSearch: true,
              treeNodeFilterProp: 'name',
            }}
          />
          <ProFormRadio.Group
            name="admin"
            label="路由类型"
            radioType="button"
            tooltip="后端:egg路由,前端:ant路由"
            initialValue={admin}
            options={[
              {
                label: '后端:egg路由',
                value: true,
              },
              {
                label: '前端:ant路由',
                value: false,
              },
            ]}
            fieldProps={{
              onChange: (e) => {
                setAdmin(e.target.value);
              },
            }}
            rules={[{ required: true }]}
          />
          <ProFormSelect
            disabled
            options={[
              {
                value: modInfo.id,
                label: modInfo.name,
              },
            ]}
            width="xs"
            name="mod_id"
            label="所属模块"
            initialValue={modInfo.id}
          />
          <ProFormSwitch
            width="xs"
            name="hideInMenu"
            label="隐藏菜单"
            tooltip="可以在菜单中不展示这个路由，包括子路由"
            initialValue={false}
          />
          <ProFormSwitch
            width="xs"
            name="role"
            label="权限节点"
            tooltip="如果是权限节点会在角色里面显示节点"
            initialValue={false}
          />
          <ProFormDigit
            width="xs"
            label="排序"
            name="sort"
            tooltip="数字越小越靠前"
            placeholder="0"
            fieldProps={{ precision: 0 }}
          />
        </ProForm.Group>

        {admin ? (
          <>
            <ProFormRadio.Group
              name="verb"
              label="用户触发动作"
              tooltip="用户触发动作，支持 get，post 等所有 HTTP 方法"
              initialValue={'get'}
              rules={[{ required: true }]}
              options={[
                {
                  label: 'get',
                  value: 'get',
                },
                {
                  label: 'post',
                  value: 'post',
                },
                {
                  label: 'put',
                  value: 'put',
                },
                {
                  label: 'del',
                  value: 'del',
                },
                {
                  label: 'resources',
                  value: 'resources',
                },
                {
                  label: 'redirect',
                  value: 'redirect',
                },
                {
                  label: 'patch',
                  value: 'patch',
                },
                {
                  label: 'head',
                  value: 'head',
                },
                {
                  label: 'options',
                  value: 'options',
                },
              ]}
              fieldProps={{
                value: verb,
                onChange: async (e) => {
                  console.log(e.target.value);
                  setVerb(e.target.value);
                },
              }}
            />
            {verb !== 'redirect' && (
              <>
                <ProForm.Group>
                  <ProFormSelect
                    width="md"
                    name="controller"
                    label="控制器"
                    request={async (): Promise<any> => {
                      const data = await adminApi.cwDev.AdminapiCwDevGetcontroller({}); //getController
                      return data.data;
                    }}
                    tooltip="egg的控制器文件"
                    placeholder="请选择控制器"
                    rules={[{ required: true }]}
                    fieldProps={{
                      showSearch: true,
                      onChange: async (e) => {
                        console.log(e);
                        const data = await adminApi.cwDev.AdminapiCwDevGetcontrollerfun({ c: e }); //getControllerFun
                        setAction(data.data);
                        setActonSelect('');
                      },
                    }}
                  />
                  {verb != 'resources' && (
                    <ProFormSelect
                      width="md"
                      name="action"
                      label="控制器方法"
                      options={action}
                      tooltip="控制器方法"
                      placeholder="请选择方法"
                      rules={[{ required: true }]}
                      fieldProps={{
                        showSearch: true,
                        value: actonSelect,
                        onChange: async (e) => {
                          setActonSelect(e);
                        },
                      }}
                    />
                  )}
                </ProForm.Group>
                <ProForm.Group>
                  <ProFormSelect
                    width="md"
                    name="middleware"
                    label="中间件"
                    request={async () => {
                      const data = await adminApi.cwDev.AdminapiCwDevGetmiddleware({});
                      return data.data;
                    }}
                    tooltip="在 Router 里面可以配置多个 Middleware"
                    placeholder="请选择Middleware"
                    fieldProps={{ mode: 'multiple' }}
                  />
                  <ProFormSelect
                    width="md"
                    name="ignoreMiddleware"
                    label="排除中间件"
                    request={async () => {
                      const data = await adminApi.cwDev.AdminapiCwDevGetmiddleware({});
                      return data.data;
                    }}
                    tooltip="排除模块统一设置的middleware"
                    placeholder="请选择Middleware"
                    fieldProps={{ mode: 'multiple' }}
                  />
                </ProForm.Group>
              </>
            )}
          </>
        ) : (
          <ProForm.Group>
            <ProFormSelect
              width="md"
              name="parentKeys"
              label="parentKeys"
              tooltip="当此节点被选中的时候也会选中 parentKeys 的节点"
              placeholder="parentKeys"
              fieldProps={{
                mode: 'tags',
                tokenSeparators: [','],
              }}
            />
            <ProFormSelect
              width="md"
              name="access"
              label="权限配置"
              tooltip="控制器方法"
              placeholder="权限配置，需要预先配置权限"
              fieldProps={{
                mode: 'tags',
                tokenSeparators: [','],
              }}
            />
            <ProFormSwitch
              width="xs"
              name="hideChildrenInMenu"
              label="隐藏子路由"
              tooltip="用于隐藏不需要在菜单中展示的子路由"
              initialValue={false}
            />
            <ProFormSwitch
              width="xs"
              name="hideInBreadcrumb"
              label="在面包屑中隐藏"
              tooltip="可以在面包屑中不展示这个路由，包括子路由"
              initialValue={false}
            />
            <ProFormSwitch
              width="xs"
              name="headerRender"
              label="显示顶栏"
              tooltip="当前路由展示顶栏"
              initialValue={true}
            />
            <ProFormSwitch
              width="xs"
              name="footerRender"
              label="显示页脚"
              tooltip="当前路由展示页脚"
              initialValue={true}
            />
            <ProFormSwitch
              width="xs"
              name="menuRender"
              label="展示菜单"
              tooltip="当前路由展示菜单"
              initialValue={true}
            />
            <ProFormSwitch
              width="xs"
              name="menuHeaderRender"
              label="显示菜单顶栏"
              tooltip="当前路由不展示菜单顶栏"
              initialValue={true}
            />
            <ProFormSwitch
              width="xs"
              name="flatMenu"
              label="子项上提"
              tooltip="子项往上提，只是不展示父菜单"
              initialValue={false}
            />
          </ProForm.Group>
        )}
      </ModalForm>

      <ModalForm
        title={'编辑路由:' + editRoutes.name}
        visible={editRoutesVisit}
        onFinish={async (values) => {
          values.id = editRoutes.id;
          console.log(values);
          const data = await adminApi.cwDev.AdminapiCwDevEditrouter({}, values);
          if (data.errorCode == '0') {
            actionRef?.current?.reload();
            message.success('提交成功');
          }
          return true;
        }}
        onVisibleChange={(visible) => {
          console.log(visible);
          setEditRoutesVisit(visible);
          if (!visible) {
            setAdmin(true);
            setVerb('get');
            setAction([]);
            setActonSelect('');
          }
        }}
        modalProps={{
          maskClosable: false,
          destroyOnClose: true,
        }}
      >
        <ProForm.Group>
          <ProFormText
            width="md"
            name="name"
            initialValue={editRoutes.name}
            label="路由名称"
            tooltip="路由名称"
            placeholder="请输入路由名称"
            rules={[{ required: true }]}
          />
          <ProFormText
            width="md"
            name="path"
            initialValue={editRoutes.path}
            label="路由URL路径"
            tooltip="路由URL路径"
            placeholder="请输入路由URL路径"
            rules={[{ required: true }]}
          />
          <ProFormSelect
            width="md"
            name="icon"
            initialValue={editRoutes.icon}
            label="路由图标"
            request={async () => {
              return await getIconFont();
            }}
            tooltip="使用 iconfont.cn"
            placeholder="请选择图标"
            fieldProps={{
              optionLabelProp: 'value',
              showSearch: true,
              optionItemRender(item) {
                return (
                  <>
                    <IconFont type={item.value} /> {item.label}
                  </>
                );
              },
            }}
          />
          <ProFormTreeSelect
            width="md"
            name="pid"
            initialValue={editRoutes.pid}
            label="上级路由"
            rules={[{ required: true }]}
            request={async (): Promise<any> => {
              const data = await adminApi.cwDev.AdminapiCwDevGetalltreemenu({
                mod_id: modInfo.id,
              });
              return data.data;
            }}
            tooltip="如果是一级菜单请选择一级菜单"
            placeholder="请选择上级路由"
            fieldProps={{
              fieldNames: {
                label: 'name',
                value: 'id',
              },
              showSearch: true,
              treeNodeFilterProp: 'name',
            }}
          />
          <ProFormRadio.Group
            name="admin"
            label="路由类型"
            radioType="button"
            tooltip="后端:egg路由,前端:ant路由"
            initialValue={admin}
            options={[
              {
                label: '后端:egg路由',
                value: true,
              },
              {
                label: '前端:ant路由',
                value: false,
              },
            ]}
            fieldProps={{
              onChange: (e) => {
                setAdmin(e.target.value);
              },
            }}
            rules={[{ required: true }]}
          />
          <ProFormSelect
            disabled
            options={[
              {
                value: modInfo.id,
                label: modInfo.name,
              },
            ]}
            width="xs"
            name="mod_id"
            label="所属模块"
            initialValue={modInfo.id}
          />
          <ProFormSwitch
            width="xs"
            name="hideInMenu"
            label="隐藏菜单"
            tooltip="可以在菜单中不展示这个路由，包括子路由"
            initialValue={editRoutes.hideInMenu}
          />
          <ProFormSwitch
            width="xs"
            name="role"
            label="权限节点"
            tooltip="如果是权限节点会在角色里面显示节点"
            initialValue={editRoutes.role}
          />
          <ProFormDigit
            width="xs"
            label="排序"
            name="sort"
            initialValue={editRoutes.sort}
            tooltip="数字越小越靠前"
            placeholder="0"
            fieldProps={{ precision: 0 }}
          />
        </ProForm.Group>

        {admin ? (
          <>
            <ProFormRadio.Group
              name="verb"
              label="用户触发动作"
              tooltip="用户触发动作，支持 get，post 等所有 HTTP 方法"
              initialValue={editRoutes.verb}
              rules={[{ required: true }]}
              options={[
                {
                  label: 'get',
                  value: 'get',
                },
                {
                  label: 'post',
                  value: 'post',
                },
                {
                  label: 'put',
                  value: 'put',
                },
                {
                  label: 'del',
                  value: 'del',
                },
                {
                  label: 'resources',
                  value: 'resources',
                },
                {
                  label: 'redirect',
                  value: 'redirect',
                },
                {
                  label: 'patch',
                  value: 'patch',
                },
                {
                  label: 'head',
                  value: 'head',
                },
                {
                  label: 'options',
                  value: 'options',
                },
              ]}
              fieldProps={{
                value: verb || editRoutes.verb,
                onChange: async (e) => {
                  console.log(e.target.value);
                  setVerb(e.target.value);
                },
              }}
            />
            {verb !== 'redirect' && (
              <>
                <ProForm.Group>
                  <ProFormSelect
                    width="md"
                    name="controller"
                    initialValue={editRoutes.controller}
                    label="控制器"
                    request={async (): Promise<any> => {
                      const data = await adminApi.cwDev.AdminapiCwDevGetcontroller({});
                      return data.data;
                    }}
                    tooltip="egg的控制器文件"
                    placeholder="请选择控制器"
                    rules={[{ required: true }]}
                    fieldProps={{
                      showSearch: true,
                      onChange: async (e) => {
                        console.log(e);
                        const data = await adminApi.cwDev.AdminapiCwDevGetcontrollerfun({ c: e });
                        setAction(data.data);
                        setActonSelect(editRoutes.action || '');
                      },
                    }}
                  />
                  {verb != 'resources' && (
                    <ProFormSelect
                      width="md"
                      name="action"
                      initialValue={actonSelect}
                      label="控制器方法"
                      options={action}
                      tooltip="控制器方法"
                      placeholder="请选择方法"
                      rules={[{ required: true }]}
                      fieldProps={{
                        showSearch: true,
                        value: actonSelect,
                        onChange: async (e) => {
                          setActonSelect(e);
                        },
                      }}
                    />
                  )}
                </ProForm.Group>
                <ProForm.Group>
                  <ProFormSelect
                    width="md"
                    name="middleware"
                    initialValue={editRoutes.middleware ? editRoutes.middleware.split(',') : []}
                    label="中间件"
                    request={async () => {
                      const data = await adminApi.cwDev.AdminapiCwDevGetmiddleware({});
                      return data.data;
                    }}
                    tooltip="在 Router 里面可以配置多个 Middleware"
                    placeholder="请选择Middleware"
                    fieldProps={{ mode: 'multiple' }}
                  />
                  <ProFormSelect
                    width="md"
                    name="ignoreMiddleware"
                    initialValue={
                      editRoutes.ignoreMiddleware ? editRoutes.ignoreMiddleware.split(',') : []
                    }
                    label="排除中间件"
                    request={async () => {
                      const data = await adminApi.cwDev.AdminapiCwDevGetmiddleware({});
                      return data.data;
                    }}
                    tooltip="排除模块统一设置的middleware"
                    placeholder="请选择Middleware"
                    fieldProps={{ mode: 'multiple' }}
                  />
                </ProForm.Group>
              </>
            )}
          </>
        ) : (
          <ProForm.Group>
            <ProFormSelect
              width="md"
              name="parentKeys"
              initialValue={editRoutes.parentKeys ? editRoutes.parentKeys.split(',') : []}
              label="parentKeys"
              tooltip="当此节点被选中的时候也会选中 parentKeys 的节点"
              placeholder="parentKeys"
              fieldProps={{
                mode: 'tags',
                tokenSeparators: [','],
              }}
            />
            <ProFormSelect
              width="md"
              name="access"
              initialValue={editRoutes.access ? editRoutes.access.split(',') : []}
              label="权限配置"
              tooltip="控制器方法"
              placeholder="权限配置，需要预先配置权限"
              fieldProps={{
                mode: 'tags',
                tokenSeparators: [','],
              }}
            />
            <ProFormSwitch
              width="xs"
              name="hideChildrenInMenu"
              label="隐藏子路由"
              tooltip="用于隐藏不需要在菜单中展示的子路由"
              initialValue={editRoutes.hideChildrenInMenu}
            />
            <ProFormSwitch
              width="xs"
              name="hideInBreadcrumb"
              label="在面包屑中隐藏"
              tooltip="可以在面包屑中不展示这个路由，包括子路由"
              initialValue={editRoutes.hideInBreadcrumb}
            />
            <ProFormSwitch
              width="xs"
              name="headerRender"
              label="显示顶栏"
              tooltip="当前路由展示顶栏"
              initialValue={editRoutes.headerRender}
            />
            <ProFormSwitch
              width="xs"
              name="footerRender"
              label="显示页脚"
              tooltip="当前路由展示页脚"
              initialValue={editRoutes.footerRender}
            />
            <ProFormSwitch
              width="xs"
              name="menuRender"
              label="显示菜单"
              tooltip="当前路由展示示菜单"
              initialValue={editRoutes.menuRender}
            />
            <ProFormSwitch
              width="xs"
              name="menuHeaderRender"
              label="菜单顶栏"
              tooltip="当前路由展示菜单顶栏"
              initialValue={editRoutes.menuHeaderRender}
            />
            <ProFormSwitch
              width="xs"
              name="flatMenu"
              label="子项上提"
              tooltip="子项往上提，只是不展示父菜单"
              initialValue={editRoutes.flatMenu}
            />
          </ProForm.Group>
        )}
      </ModalForm>
    </PageContainer>
  );
};
