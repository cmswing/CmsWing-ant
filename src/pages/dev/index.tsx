import { history } from '@umijs/max';
import React, { useEffect } from 'react';
const Index: React.FC = () => {
  useEffect(() => {
    history.push('/dev/menu');
  }, []);
  return <></>;
};
export default Index;
