// import { outLogin } from '@/services/ant-design-pro/api';
import adminApi from '@/services/adminApi';
import { SyncOutlined } from '@ant-design/icons';
import { Button, message, Space, Tooltip } from 'antd';
import React, { useCallback, useState } from 'react';
const Restart: React.FC = () => {
  const [loadings, setLoadings] = useState<boolean>(false);
  const onRestartClick = useCallback(async () => {
    setLoadings(true);
    message.loading({ content: '服务重启中...', key: 'restart', duration: 20 });
    await adminApi.cwDev.AdminapiCwDevRestart({});
    setLoadings(false);
    message.success({ content: '重启成功!', key: 'restart', duration: 2 });
  }, [setLoadings]);
  return (
    <Space>
      <Tooltip title="重启服务">
        <Button
          type="dashed"
          shape="circle"
          icon={<SyncOutlined />}
          loading={loadings}
          onClick={onRestartClick}
        />
      </Tooltip>
    </Space>
  );
};

export default Restart;
